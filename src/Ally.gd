extends Unit

class_name Ally

func set_type(new_unit_type):
	unit_type = new_unit_type
	if new_unit_type == "bomber":
		health = 3
	elif new_unit_type == "archer":
		health = 1
	elif new_unit_type == "fountain":
		health = 2
	elif new_unit_type == "lightning":
		health = 1
	elif new_unit_type == "dragon":
		health = 4
	else: print("ERROR: bad unit type " + str(new_unit_type))
	$AnimatedSprite2D.play(unit_type)

func _ready():
	super._ready()
