extends Unit

class_name Enemy

func _ready():
	# Set random type
	unit_type = G.enemies[randi() % G.enemies.size()]
	set_type(unit_type)
	super._ready()

func set_type(new_unit_type):
	unit_type = new_unit_type
	if unit_type == "rhino":
		if G.difficulty == G.Difficulty.EASY:
			health = 2
		else:
			health = 3
	elif unit_type == "spider":
		health = 1
	elif unit_type == "snake":
		health = 2
	else:
		print("Error: Unsupported enemy " + str(unit_type))
		assert(false)
	$AnimatedSprite2D.play(unit_type)
	update_health_ui()
