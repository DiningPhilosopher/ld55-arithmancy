extends Node

var devmode = false
var automode = true
var handling_round = false
var difficulty = Difficulty.EASY
var turn = 0


enum Difficulty {
	EASY,
	MEDIUM,
	HARD,
	WILD
}

var lose_invaders = 5
var game_params  = {
	Difficulty.EASY: {
		"turns": 15,
		"max_num": 6,
	},
	Difficulty.MEDIUM: {
		"turns": 20,
		"max_num": 7,
		"levels": [
			{
				"free": 2,
				"p1": 0.2,
			}
		]
	},
	Difficulty.HARD: {
		"turns": 25,
		"max_num": 8,
		"levels": [
			{
				"free": 2,
				"p1": 0.2,
			}
		]
	},
	Difficulty.WILD: {
		"turns": 30,
		"max_num": 8,
		"levels": [
			{
				"free": 2,
				"p1": 0.2,
			}
		]
	}
}

var tile_width = 180
var tile_height_visible = 180
var tile_height_full = 180

var num_textures = {
	1: preload("res://assets/drawings/numbers/1.svg"),
	2: preload("res://assets/drawings/numbers/2.svg"),
	3: preload("res://assets/drawings/numbers/3.svg"),
	4: preload("res://assets/drawings/numbers/4.svg"),
	5: preload("res://assets/drawings/numbers/5.svg"),
	6: preload("res://assets/drawings/numbers/6.svg"),
	7: preload("res://assets/drawings/numbers/7.svg"),
	8: preload("res://assets/drawings/numbers/8.svg"),
}

var allies = [
	"bomber",
	"fountain",
	"archer",
	#"lightning",
	"dragon",
]

var enemies = [
	"rhino",
	"snake",
	"spider",
]

enum GamePhase {
	PLAYER,
	ALLY,
	ENEMY
}

func coord_to_pos(c):
	return Vector2(c.x * 180., c.y * 180.)

func parabola(p0, p1, height):
	var curve = Curve2D.new()
	var halfdx = (p1.x - p0.x) / 2
	curve.add_point(p0, Vector2(0, -height / 2))
	curve.add_point((p0 + p1) / 2 - Vector2(0, height),
			Vector2(-halfdx / 2, 0),
			Vector2(halfdx / 2, 0) )
	curve.add_point(p1, Vector2(), Vector2(0, -height / 2))
	return curve

# 4-connectivity neighbors, omitting out-of-bounds
func neighs4(coord):
	var result = []
	if coord.x > 0:
		result.append(coord + Vector2(-1, 0))
	if coord.x < 7:
		result.append(coord + Vector2(1, 0))
	if coord.y > 0:
		result.append(coord + Vector2(0, -1))
	if coord.y < 3:
		result.append(coord + Vector2(0, 1))
	return result

func neighs5(coord):
	var result = neighs4(coord)
	result.append(coord)
	return result

func neighs8(coord):
	var result = neighs4(coord)
	if coord.x > 0 and coord.y > 0:
		result.append(coord + Vector2(-1, -1))
	if coord.x < 7 and coord.y < 3:
		result.append(coord + Vector2(1, 1))
	if coord.x > 0 and coord.y < 3:
		result.append(coord + Vector2(-1, 1))
	if coord.x < 7 and coord.y > 0:
		result.append(coord + Vector2(1, -1))
	return result
