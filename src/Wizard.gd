extends Sprite2D


func set_dots(dots):
	var invisibly = 5 - dots
	for dot in $Dots.get_children():
		if invisibly > 0:
			dot.modulate.a = 0.
			invisibly -= 1
		else:
			dot.modulate.a = 1.
