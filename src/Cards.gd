extends HBoxContainer

func nums_for_difficulty():
	var nums
	if G.difficulty == G.Difficulty.EASY:
		nums = range(4, 11)
	elif G.difficulty == G.Difficulty.MEDIUM:
		nums = range(4, 13)
	else:
		nums = range(4, 15)
	return nums

func shuffle_cards():
	# pick 3 distinct numbers in the range [4, 14] (leave out 2, 3, 15 and 16
	var nums = nums_for_difficulty()
	nums.shuffle()
	var ally_types = G.allies.duplicate()
	ally_types.shuffle()
	var card_idx = 0
	for card: Card in get_children():
		card.set_number(nums[card_idx])
		card.set_unit(ally_types[card_idx])
		card_idx += 1

func replace_card(card: Card):
	var allowed_nums = nums_for_difficulty()
	var existing_types = []
	for existing in get_children():
		allowed_nums.erase(existing.number)
		existing_types.append(existing.unit)
	card.set_number(allowed_nums.pick_random())
	var allowed_unit_types = G.allies.duplicate()
	for existing in existing_types:
		allowed_unit_types.erase(existing)
	card.set_unit(allowed_unit_types.pick_random())

func replace_summon_card():
	for card in get_children():
		if card.being_summoned:
			replace_card(card)
