extends Node2D

signal attempt_sum(summed)
signal summon_complete
signal unit_processed
signal all_units_processed
signal lose_signal
signal next_turn_signal
signal update_wizard(num)

var s_tile_summon = preload("res://scenes/TileSummon.tscn")
var s_tile_forest = preload("res://scenes/TileForest.tscn")
var s_ally = preload("res://scenes/Ally.tscn")
var s_enemy = preload("res://scenes/Enemy.tscn")
var s_bomb = preload("res://scenes/Bomb.tscn")
var s_arrow = preload("res://scenes/Arrow.tscn")
var s_venom = preload("res://scenes/Venom.tscn")
var tex_fire = preload("res://assets/drawings/fire_breath.svg")

var selected_0: Node2D = null
var selected_1: Node2D = null
# Used to remember which unit to summon after all other animations finish
var summon_tile = null
var summon_type = null

# Increases probability of spawning
var spawns_last_turn = 0

var tiles = {}
#var tween: Tween

func _ready():
	place_tiles(8, 4)

func place_tiles(w, h):
	var numbers = []
	var max_num = G.game_params[G.difficulty]["max_num"]
	var full_list = range(1, max_num + 1)
	var full_times = 16 / len(full_list)
	for _i in range(full_times):
		numbers += full_list
	full_list.shuffle()
	numbers += full_list.slice(0, 16 - len(numbers))
	numbers.shuffle()
	for r in h:
		for c in w:
			var tile
			if c < 4:
				# Player area
				tile = s_tile_summon.instantiate()
				#var number = 1 + randi() % 8
				var number = numbers.pop_back()
				if G.devmode:
					tile.get_node("Hatch").modulate.a = 0.7
				tile.set_number(number)
			else:
				# Spawning area
				tile = s_tile_forest.instantiate()
			tile.set_coord(Vector2(c, r))
			$Tiles.add_child(tile)
			tiles[Vector2(c, r)] = tile
	if G.devmode:
		selected_0 = tiles[Vector2(0, 0)]
		selected_1 = tiles[Vector2(1, 1)]
		queue_summon("bomber")
		do_summon()
		summon_complete.emit()
		selected_0 = null
		selected_1 = null
		for _i in range(6):
			spawn_enemy()

func handle_selection(selected_tile):
	if G.handling_round:
		# Cannot interact while the round is resolved
		return
	if selected_tile.unit != null:
		# Cannot interact with occupied tiles
		return
	if selected_1 != null:
		# Already two selected, wait for next turn
		return
	elif selected_0 == null:
		selected_0 = selected_tile
		selected_0.open_hatch()
	else:
		if selected_0 == selected_tile:
			# Cannot select same tile twice
			return
		# Hands off now, wait for the round to finish
		G.handling_round = true
		selected_1 = selected_tile
		selected_1.hatch_opened.connect(hatches_opened, CONNECT_ONE_SHOT)
		selected_1.open_hatch()

func hatches_opened():
	var summed = selected_0.number + selected_1.number
	attempt_sum.emit(summed)

func process_turn(success: bool):
	var tween = create_tween()
	var hatch_close_time = 1.0
	var item_0_copy
	var item_1_copy
	if success:
		hatch_close_time = 1.0
		var item_0_original = selected_0.get_node("Item")
		item_0_copy = item_0_original.duplicate()
		#item_0_original.hide()
		item_0_copy.position = selected_0.position
		var item_1_original = selected_1.get_node("Item")
		item_1_copy = item_1_original.duplicate()
		#item_1_original.hide()
		item_1_copy.position = selected_1.position
		$AnimatedItems.add_child(item_1_copy)
		$AnimatedItems.add_child(item_0_copy)
	
	tween.tween_callback(close_hatches).set_delay(hatch_close_time)
	
	if success:
		#tween.tween_callback(replace_selected_items)
		var pos_tween_time = (item_0_copy.position - item_1_copy.position).length() * 0.0015
		tween.tween_property(item_0_copy, "position", item_1_copy.position, pos_tween_time).set_delay(0.4)
		var fade_time = 0.2
		tween.tween_property(item_0_copy, "modulate:a", 0., fade_time)
		tween.parallel().tween_property(item_1_copy, "modulate:a", 0., fade_time)
	tween.tween_callback(func():
		if success:
			do_summon()
			summon_complete.emit()
		selected_0 = null
		selected_1 = null
	).set_delay(0.2)
	await tween.finished
	await process_units()
	#tween = create_tween()
	#await all_units_processed
	update_fountains()
	#tween.tween_callback(update_fountains)
	#await tween.finished
	for victim in $Allies.get_children():
		if victim.dead:
			victim.queue_free()
	for victim in $Enemies.get_children():
		if victim.dead:
			victim.queue_free()
	update_wizard.emit(max(0, 5 - count_invaders()))
	if lose_condition():
		emit_signal("lose_signal")
	else:
		spawn_enemies()
	G.handling_round = false
	next_turn_signal.emit()

func spawn_enemies():
	var dif = G.difficulty
	if dif == G.Difficulty.EASY:
		if G.turn in [1, 4, 5, 7, 8, 9]:
			spawn_enemy()
		elif G.turn > 3:
			if spawns_last_turn == 0 and randf() < 0.5:
				spawn_enemy()
	elif dif == G.Difficulty.MEDIUM:
		if G.turn == 1:
			spawn_enemy()
		elif G.turn > 3:
			if G.turn in [14, 15, 16] or (spawns_last_turn == 0 and randf() < 0.8):
				spawn_enemy()
	elif dif == G.Difficulty.HARD:
		if G.turn == 1:
			spawn_enemy()
		elif G.turn > 3:
			if spawns_last_turn == 0:
				var roll = randf()
				if roll < 0.1:
					spawn_enemy()
					spawn_enemy()
				elif roll < 0.5:
					spawn_enemy()
			else:
				if randf() < 0.5:
					spawn_enemy()
	elif dif == G.Difficulty.WILD:
		if G.turn == 1:
			spawn_enemy()
			spawn_enemy()
		elif G.turn > 3:
			if spawns_last_turn == 0:
				var roll = randf()
				if roll < 0.2:
					spawn_enemy()
					spawn_enemy()
				elif roll < 0.4:
					spawn_enemy()
			else:
				if randf() < 0.5:
					spawn_enemy()

func count_invaders():
	var invaders = 0
	for c in range(4):
		for r in range(4):
			var tile_unit = tiles[Vector2(c, r)].unit
			if tile_unit is Enemy:
				invaders += 1
	return invaders
	
func lose_condition():
	return count_invaders() >= G.lose_invaders

func autoplay():
	var spawnable = []
	for r in range(4):
		for c in range(4):
			if tiles[Vector2(c, r)].unit == null:
				spawnable.append(tiles[Vector2(c, r)])
	if spawnable.size() < 2:
		reset()
		if G.automode:
			autoplay()
		return
	spawnable.shuffle()
	handle_selection(spawnable.pop_front())
	handle_selection(spawnable.pop_front())

func reset():
	summon_tile = null
	summon_type = null
	for collection in [$Allies, $Enemies, $AnimatedItems]:
		for child in collection.get_children():
			child.queue_free()
	for coord in tiles:
		tiles[coord].queue_free()
	tiles = {}
	place_tiles(8, 4)

func replace_selected_items():
	var nums = range(1, 9)
	nums.shuffle()
	selected_0.set_number(nums.pop_front())
	selected_1.set_number(nums.pop_front())
	selected_0.set_seen(false)
	selected_1.set_seen(false)

# Wait for hatches to close, then do_summon will take over
func queue_summon(unit_type):
	self.summon_tile = selected_1
	self.summon_type = unit_type

func do_summon():
	$Summon.play()
	if summon_type == null:
		assert(summon_tile == null)
		return
	var new_unit = s_ally.instantiate()
	new_unit.set_type(summon_type)
	summon_tile.set_unit(new_unit)
	$Allies.add_child(new_unit)
	summon_tile = null
	summon_type = null
	update_fountains()

func spawn_enemy():
	var spawn_zone = []
	var leftmost = 6
	if G.devmode:
		leftmost = 4
	for r in range(4):
		for c in range(leftmost, 8):
			var tile = tiles[Vector2(c, r)]
			if tile.unit == null:
				spawn_zone.push_back(tile)
	if spawn_zone.is_empty():
		return
	var spawn_tile = spawn_zone.pick_random()
	var new_unit: Enemy = s_enemy.instantiate()
	spawn_tile.set_unit(new_unit)
	$Enemies.add_child(new_unit)

func close_hatches():
	selected_0.close_hatch()
	selected_1.close_hatch()

func process_units():
	for unit in $Allies.get_children():
		if unit != null and not unit.dead and not unit.unit_type == "fountain":
			unit.get_node("AnimatedSprite2D").modulate = Color.WHITE
			await process_ally(unit)
	for unit in $Enemies.get_children():
		if unit != null and not unit.dead:
			unit.get_node("AnimatedSprite2D").modulate = Color.WHITE
			await process_enemy(unit)

func process_ally(unit):
	if not unit.dead:
		if unit.unit_type == "bomber":
			await process_bomber(unit)
		elif unit.unit_type == "archer":
			await process_archer(unit)
		elif unit.unit_type == "dragon":
			await process_dragon(unit)
		else:
			var aween = create_tween()
			aween.tween_interval(0.1)
			await aween.finished
			print("ERROR: unsupported ally type: " + str(unit))

func process_enemy(unit):
	if unit.unit_type == "rhino":
		await process_rhino(unit)
	elif unit.unit_type == "snake":
		await process_snake(unit)
	elif unit.unit_type == "spider":
		await process_spider(unit)
	else:
		print("ERROR: unsupported enemy type: " + str(unit.unit_type))

func process_bomber(unit):
	var bween = create_tween()
	# Select tile to bomb
	var unit_coord = unit.tile.coord
	var unit_pos = G.coord_to_pos(unit_coord)
	var bomb_distance = 1 + randi() % 4
	var bomb_coord = unit_coord + Vector2(bomb_distance, 0)
	var bomb_pos = G.coord_to_pos(bomb_coord)
	var bomb = s_bomb.instantiate()
	var curve = G.parabola(unit_pos, bomb_pos, 150)
	var path2d = Path2D.new()
	path2d.set_curve(curve)
	$AnimatedItems.add_child(path2d)
	var follow = PathFollow2D.new()
	follow.rotates = false
	path2d.add_child(follow)
	follow.add_child(bomb)
	bween.tween_method(
		func(x):
			var parav = 0.5 * x + 2. * pow((x-0.5), 3) + 0.25
			follow.progress_ratio = parav,
			0., 1., 0.7
	)
	bween.tween_callback(func():
		bomb.explode())
	await bomb.explosion_done
	var neigh_coords = G.neighs5(bomb_coord)
	for neigh_coord in neigh_coords:
		var neigh_unit = tiles[neigh_coord].unit
		if neigh_unit != null:
			await neigh_unit.take_dmg(1)

func process_archer(unit):
	var chween = create_tween()
	var unit_coord = unit.tile.coord
	var unit_pos = G.coord_to_pos(unit_coord)
	# Find target on the left
	var target_coord = unit_coord
	var target_unit = null
	for c in range(unit_coord.x - 1, -1, -1):
		target_coord = Vector2(c, unit_coord.y)
		if tiles[target_coord].unit is Enemy:
			target_unit = tiles[target_coord].unit
			break
	if target_unit == null:
		chween.tween_interval(0.1)
	else:
		var target_pos = G.coord_to_pos(target_coord)
		var arrow = s_arrow.instantiate()
		arrow.position = unit_pos
		$AnimatedItems.add_child(arrow)
		chween.set_trans(Tween.TRANS_LINEAR)
		var duration = (unit_coord.x - target_coord.x) * 0.1
		chween.tween_property(arrow, "position", target_pos, duration)
		chween.tween_property(arrow, "modulate:a", 0.0, 0.08)
		chween.tween_callback(func():
			await target_unit.take_dmg(2)
			arrow.queue_free())
	await chween.finished

func process_dragon(unit):
	var unit_coord = unit.tile.coord
	# Check if has enemy on the right
	var neigh_coord = unit_coord + Vector2(1, 0)
	var neigh_unit = tiles[neigh_coord].unit
	if neigh_unit != null and neigh_unit is Enemy:
		var fire = Sprite2D.new()
		fire.texture = tex_fire
		fire.offset = Vector2(90, -5)
		fire.position = G.coord_to_pos(unit_coord)
		$AnimatedItems.add_child(fire)
		var dween = create_tween()
		dween.tween_property(fire, "modulate:a", 1., 0.1)
		dween.tween_interval(0.5)
		dween.tween_property(fire, "modulate:a", 0., 0.3)
		dween.tween_callback(await neigh_unit.take_dmg.bind(3))
		await dween.finished

func update_fountains():
	# First reset bonus hp everywhere
	for key in tiles:
		var unit = tiles[key].unit
		if unit != null:
			unit.bonus_hp = 0
	# Then reapply the bonus
	for key in tiles:
		var tile = tiles[key]
		var unit = tile.unit
		if unit != null and unit.unit_type == "fountain":
			var neighs = G.neighs8(tile.coord)
			for neigh in neighs:
				var neigh_tile = tiles[neigh]
				var neigh_unit = neigh_tile.unit
				if neigh_unit is Ally:
					neigh_unit.bonus_hp = 1
	# Finally update the health UI everywhere
	for key in tiles:
		var unit = tiles[key].unit
		if unit != null:
			unit.update_health_ui()
			unit.maybe_die()

func process_rhino(unit):
	var unit_coord = unit.tile.coord
	# No eligible Ally to attack, move normally.
	var next_coord = unit_coord - Vector2(1, 0)
	# If occupied by ally, attack. Otherwise, do nothing.
	if next_coord.x == -1:
		# End of the line, stay here
		return
	var neigh_unit = tiles[next_coord].unit
	var own_pos = G.coord_to_pos(unit_coord)
	var neigh_pos = G.coord_to_pos(next_coord)
	if neigh_unit == null:
		var rween = create_tween()
		# Tile to the left is empty, find nearest ally on the left to charge
		var charge_coord = unit_coord
		var charge_unit
		var no_charge = false
		while charge_coord.x > 0:
			charge_coord -= Vector2(1, 0)
			var charge_tile = tiles[charge_coord]
			charge_unit = charge_tile.unit
			if charge_tile != null:
				if charge_unit is Enemy:
					no_charge = true
					break
				elif charge_unit is Ally:
					break
		if no_charge or charge_unit == null:
			# No clear charge path, just walk to the left
			rween.tween_property(unit, "position", neigh_pos, 0.4)
			rween.tween_callback(func():
				tiles[next_coord].set_unit(unit))
		else:
			# Charge!
			var charge_pos = G.coord_to_pos(charge_coord)
			var charge_time = (unit_coord.x - charge_coord.x) * 0.2
			rween.tween_property(unit, "position", charge_pos, charge_time)
			rween.tween_callback(await charge_unit.take_dmg.bind(1))
			var new_coord = charge_coord + Vector2(1, 0)
			rween.tween_property(unit, "position", G.coord_to_pos(new_coord), 0.4)
			rween.tween_callback(func():
				tiles[new_coord].set_unit(unit))
		await rween.finished
	elif neigh_unit is Ally:
		var rween = create_tween()
		# Attack!
		rween.tween_property(unit, "position", neigh_pos, 0.4)
		rween.tween_callback(await neigh_unit.take_dmg.bind(1))
		rween.tween_property(unit, "position", own_pos, 0.4)
		await rween.finished
	elif neigh_unit is Enemy:
		return
	else:
		print("ERROR: unexpected neighbor unit: " + str(neigh_unit))

func process_snake(unit):
	var sween = create_tween()
	var unit_coord = unit.tile.coord
	var unit_pos = G.coord_to_pos(unit_coord)
	# First shoot: find target on the left
	var target_coord = unit_coord
	var target_unit = null
	for c in range(unit_coord.x - 1, -1, -1):
		target_coord = Vector2(c, unit_coord.y)
		if tiles[target_coord].unit is Ally:
			target_unit = tiles[target_coord].unit
			break
	if target_unit == null:
		sween.tween_interval(0.1)
	else:
		var target_pos = G.coord_to_pos(target_coord)
		var venom = s_venom.instantiate()
		venom.position = unit_pos
		$AnimatedItems.add_child(venom)
		sween.set_trans(Tween.TRANS_LINEAR)
		var duration = (unit_coord.x - target_coord.x) * 0.15
		sween.tween_property(venom, "position", target_pos, duration)
		sween.tween_property(venom, "modulate:a", 0.0, 0.15)
		sween.tween_callback(func():
			await target_unit.take_dmg(1)
			venom.queue_free())
	var next_coord = unit_coord - Vector2(1, 0)
	# Move if the tile is free
	sween.set_trans(Tween.TRANS_LINEAR)
	if next_coord.x == -1:
		# Just stay here
		sween.tween_interval(0.1)
		await sween.finished
		return
	var neigh_unit = tiles[next_coord].unit
	var own_pos = G.coord_to_pos(unit_coord)
	var neigh_pos = G.coord_to_pos(next_coord)
	if neigh_unit == null:
		sween.tween_property(unit, "position", neigh_pos, 0.4)
		sween.tween_callback(func():
			tiles[next_coord].set_unit(unit))
	await sween.finished

func process_spider(unit):
	var unit_coord = unit.tile.coord
	var unit_pos = G.coord_to_pos(unit_coord)
	# Check if ally on left or right, in that case attack
	var left_unit
	var left_coord = unit_coord - Vector2(1, 0)
	if left_coord.x >= 0:
		left_unit = tiles[left_coord].unit
	var right_unit
	var right_coord = unit_coord + Vector2(1, 0)
	if right_coord.x < 8:
		right_unit = tiles[right_coord].unit
	var neigh_pos
	var neigh_unit = null
	if left_unit != null and left_unit is Ally:
		neigh_pos = G.coord_to_pos(left_coord)
		neigh_unit = left_unit
	elif right_unit != null and right_unit is Ally:
		neigh_pos = G.coord_to_pos(right_coord)
		neigh_unit = right_unit
	if neigh_unit != null:
		var tween = create_tween()
		tween.tween_property(unit, "position", neigh_pos, 0.4)
		tween.tween_callback(await neigh_unit.take_dmg.bind(1))
		tween.tween_property(unit, "position", unit_pos, 0.4)
		await tween.finished
	else:
		# Try to teleport by spider string
		var tp_tiles = []
		for coord in tiles:
			if coord.x < 4 and tiles[coord].unit == null:
				tp_tiles.append(tiles[coord])
		if not tp_tiles.is_empty():
			var target_tile = tp_tiles.pick_random()
			var target_pos = G.coord_to_pos(target_tile.coord)
			var tween = create_tween()
			tween.tween_property(unit, "position:y", -300, 0.8)
			tween.tween_callback(func(): unit.position.x = target_pos.x)
			tween.tween_property(unit, "position:y", target_pos.y, 0.8)
			tween.tween_callback(func():
				target_tile.set_unit(unit))
			await tween.finished
		else:
			# Walk to the left
			var next_coord = unit_coord - Vector2(1, 0)
			# If occupied by ally, attack. Otherwise, do nothing.
			if next_coord.x == -1:
				# End of your rope, stay here
				return
			if neigh_unit == null:
				var tween = create_tween()
				tween.tween_property(unit, "position", neigh_pos, 0.4)
				tween.tween_callback(func():
					tiles[next_coord].set_unit(unit))
				await tween.finished

func mark_unit_tiles():
	return
	for key in tiles:
		var tile = tiles[key]
		var to_modulate = tile
		if tile is TileSummon:
			to_modulate = tile.get_node("Hatch")
		if tile.unit != null:
			to_modulate.modulate.g = 0.
		else:
			to_modulate.modulate.g = 1.
