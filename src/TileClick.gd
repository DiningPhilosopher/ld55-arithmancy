extends Area2D

signal select_tile(number_picked)

func _input_event(_vp, event, _shape_idx):
	if event is InputEventMouseButton and event.button_index == MOUSE_BUTTON_LEFT and event.pressed:
		select_tile.emit($"..")
