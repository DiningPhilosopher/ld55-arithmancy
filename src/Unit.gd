extends Node2D

class_name Unit

var s_dot_green = preload("res://scenes/DotGreen.tscn")
var s_dot_blue = preload("res://scenes/DotBlue.tscn")

var tween
var anim_spr
var unit_type
var tile
var dead  # Used to avoid freeing nodes in the middle of things.

var bonus_hp
var health

func _init():
	bonus_hp = 0
	dead = false
	
func _ready():
	anim_spr = $AnimatedSprite2D
	update_health_ui()

func set_tile(new_tile):
	tile = new_tile

func update_health_ui():
	for dot in $Health.get_children():
		dot.hide()
		dot.queue_free()
	for _i in range(5 - (health + bonus_hp)):
		var empty_dot = Control.new()
		empty_dot.custom_minimum_size = Vector2(12, 12)
		$Health.add_child(empty_dot)
	for _i in range(max(0, health)):
		$Health.add_child(s_dot_green.instantiate())
	for _i in range(bonus_hp):
		$Health.add_child(s_dot_blue.instantiate())

func take_dmg(dmg):
	tween = create_tween()
	if dmg > health:
		health = 0
		var remainder_dmg = (dmg - health)
		if remainder_dmg > bonus_hp:
			bonus_hp = 0
		else:
			bonus_hp -= remainder_dmg
	else:
		health -= dmg
	# Update dots
	update_health_ui()
	# Wait for it
	tween.tween_interval(0.5)
	maybe_die()
	await tween.finished
	

func maybe_die():
	if health == 0 and bonus_hp == 0:
		tile.set_unit(null)
		dead = true
