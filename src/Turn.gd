extends TextureRect

signal win_signal

var turn: int

func _ready():
	set_turn(1)

func next_turn():
	if turn < G.game_params[G.difficulty]["turns"]:
		set_turn(turn + 1)
	else:
		win_signal.emit()

func set_turn(n: int):
	turn = n
	$Center/Text.text = "TURN " + str(n) + "/" + str(G.game_params[G.difficulty]["turns"])
	G.turn = turn
