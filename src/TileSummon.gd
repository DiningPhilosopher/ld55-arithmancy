extends Tile

class_name TileSummon

signal hatch_opened

var number: int
var seen: bool

func _ready():
	var arena = $"../.."
	$Clickable.select_tile.connect(arena.handle_selection)
	$Hatch.named_animation_finished.connect(on_hatch_animation_finished)
	seen = false

func set_number(new_number):
	number = new_number
	$Item.texture = G.num_textures[number]

func set_seen(new_seen: bool):
	self.seen = new_seen

func open_hatch():
	$click_open.play()
	$Hatch.play("open")
	await $Hatch.animation_finished

func close_hatch():
	$click_close.play()
	$Hatch.play("close")
	await $Hatch.animation_finished
	
func on_hatch_animation_finished(anim_name):
	if anim_name == "close":
		$Item.show()
	if anim_name == "open":
		$Item.show()
		hatch_opened.emit()
