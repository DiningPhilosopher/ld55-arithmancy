extends Node2D

class_name Tile

var unit
var coord

func set_coord(new_coord):
	coord = new_coord
	position = G.coord_to_pos(new_coord)

func set_unit(new_unit):
	if new_unit != null:
		if unit != null:
			print("ERROR placing new unit at " + str(coord) + ", tile is already occupied!")
			assert(false)
	# Wherever the new unit was before, it should no longer have it.
		if new_unit.tile != null:
			new_unit.tile.set_unit(null)
		new_unit.position = position
		new_unit.set_tile(self)
	unit = new_unit
