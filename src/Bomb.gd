extends Sprite2D

signal explosion_done

var tween

var explode_tex = preload("res://assets/drawings/explosion.svg")

func _ready():
	explosion_done.connect(
		queue_free
	)

func explode():
	self.texture = explode_tex
	tween = create_tween()
	tween.tween_property(self, "scale", Vector2(1., 1.), 0.1).from(Vector2(0.3, 0.3))
	tween.tween_property(self, "modulate:a", 0.1, 0.4)
	tween.tween_callback(func():
		explosion_done.emit())
