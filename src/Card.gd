extends CenterContainer

class_name Card

var number
var unit

var being_summoned

func _ready():
	being_summoned = false

func set_number(new_number):
	number = new_number
	$Symbols/NumberContainer/Number.text = str(new_number)

func set_unit(new_unit):
	being_summoned = false
	material.set_shader_parameter("progress", 0.)
	unit = new_unit
	var anim: AnimatedSprite2D = $Symbols/MonsterContainer/Monster
	if unit == "bomber":
		anim.offset.y = -30
	elif unit == "dragon":
		anim.offset.y = -20
	elif unit == "fountain":
		anim.offset.y = -20
	elif unit == "archer":
		anim.offset.y = -25
	anim.play(unit)

func set_summoned():
	being_summoned = true
	material.set_shader_parameter("progress", 1.)
