extends TextureRect

signal play_difficulty(difficulty: G.Difficulty)

var tex_intro = preload("res://assets/drawings/menu/menu_start.png")
var tex_lose = preload("res://assets/drawings/menu/menu_lose.svg")
var tex_win = preload("res://assets/drawings/menu/menu_win.svg")

var play_btns

func _ready():
	play_btns = $CenterContainer/PlayAgain/PlayBtns
	var names = ["EASY", "NORMAL", "HARD", "WILD"]
	var difficulties = [
		G.Difficulty.EASY,
		G.Difficulty.MEDIUM,
		G.Difficulty.HARD,
		G.Difficulty.WILD,
	]
	for btn in play_btns.get_children():
		btn.get_node("CenterContainer/Text").text = names.pop_front()
		var difficulty = difficulties.pop_front()
		btn.pressed.connect(fade_to_play.bind(difficulty))

func fade_to_play(difficulty):
	set_menu_interactable(false)
	var tween = create_tween()
	tween.tween_property(self, "modulate:a", 0.0, 0.5)
	tween.tween_callback(func():
		hide()
		modulate.a = 1.0
		play_difficulty.emit(difficulty))

func set_menu_interactable(value):
	var do_disable = not value
	for btn in play_btns.get_children():
		btn.disabled = do_disable;
	$CenterContainer/NextBtn/TextureButton.disabled = do_disable

func show_lose():
	$CenterContainer/PlayAgain/Prompt.show()
	set_menu_interactable(true)
	texture = tex_lose
	$CenterContainer/NextBtn.hide()
	$CenterContainer/PlayAgain.show()
	show()

func show_next():
	set_menu_interactable(true)
	texture = tex_win
	$CenterContainer/NextBtn.show()
	$CenterContainer/PlayAgain.hide()
	show()
	
func show_win():
	$CenterContainer/PlayAgain/Prompt.show()
	set_menu_interactable(true)
	texture = tex_win
	$CenterContainer/NextBtn.hide()
	$CenterContainer/PlayAgain.show()
	show()
