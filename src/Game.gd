extends Node2D

var tween: Tween

func _init():
	pass
	#seed(1)

func _ready():
	$Arena.attempt_sum.connect(attempt_sum)
	$Arena.summon_complete.connect($Cards.replace_summon_card)
	$Arena.next_turn_signal.connect(next_turn)
	$Arena.lose_signal.connect(lose)
	$Arena.update_wizard.connect($Wizard.set_dots)
	$Turn.win_signal.connect(win_level)
	$Cards.shuffle_cards()
	$Menu/CenterContainer/NextBtn/TextureButton.pressed.connect(reset)
	$Menu.play_difficulty.connect(play_game)
	$Concede.pressed.connect(lose)

func play_game(difficulty):
	G.difficulty = difficulty
	G.handling_round = false
	reset()

func next_turn():
	$Turn.next_turn()
	if not $"Menu".visible and G.automode:
		$Arena.autoplay()

func reset():
	G.turn = 0
	$Arena.reset()
	$Cards.shuffle_cards()
	$Turn.set_turn(1)
	
func lose():
	$LoseSound.play()
	$Menu.show_lose()

func win_level():
	$WinSound.play()
	$Menu.show_win()


func attempt_sum(summed):
	var success = false
	for card in $Cards.get_children():
		if summed == card.number:
			$Arena.queue_summon(card.unit)
			card.set_summoned()
			success = true
			break
	$Arena.process_turn(success)


