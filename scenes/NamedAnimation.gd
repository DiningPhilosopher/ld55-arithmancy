extends AnimatedSprite2D


signal named_animation_finished(animation_name)

func on_animation_finished():
	emit_signal("named_animation_finished", animation)

func _ready():
	animation_finished.connect(on_animation_finished)
